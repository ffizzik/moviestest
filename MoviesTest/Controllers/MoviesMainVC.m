//
//  MoviesMainVC.m
//  MoviesTest
//
//  Created by Eugene Fozekosh on 1/30/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

#import "MoviesMainVC.h"
#import "MovieTableViewCell.h"
#import "Movie.h"
#import "NetworkManager.h"
#import "MovieDetailVC.h"

static NSString *const kDetailVCSegueIdentifier = @"toDetailMovieVC";

@interface MoviesMainVC () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *moviesTableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchBarTopConstraint; // 0 by default

@property (nonatomic, strong) NSMutableArray *movies;
@property (nonatomic, assign) NSInteger pageNumber;

@end


@implementation MoviesMainVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.
    self.pageNumber = 1;
    [self loadMoreTopMoviesFromPage:[NSNumber numberWithInteger:self.pageNumber]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.searchBar resignFirstResponder];
}

- (void)loadMoreSearchMoviesFromPage:(NSNumber*)pageNumber
{
    [[NetworkManager sharedInstance] getMoviesWithSearchQuery:self.searchBar.text page:pageNumber completion:^(NSArray *dataArray) {
        if (self.movies.count>0)
        {
            [self.movies addObjectsFromArray:dataArray];
        }
        else
        {
            self.movies = [NSMutableArray arrayWithArray:dataArray];
        }
        
        [self.moviesTableView reloadData];
    }];
}


- (void)loadMoreTopMoviesFromPage:(NSNumber*)pageNumber
{
    [[NetworkManager sharedInstance] getTopRatedMoviesWithPage:pageNumber completion:^(NSArray *dataArray) {

        if (self.movies.count>0)
        {
            [self.movies addObjectsFromArray:dataArray];
        }
        else
        {
            self.movies = [NSMutableArray arrayWithArray:dataArray];
        }
        
        [self.moviesTableView reloadData];
    }];
}
- (IBAction)onSearchButtonTapped:(UIBarButtonItem*)sender
{
    if (self.searchBarTopConstraint.constant < 0)
    {
        self.searchBarTopConstraint.constant = 0;
        [self.searchBar becomeFirstResponder];
    }
    else
    {
        self.searchBarTopConstraint.constant = -CGRectGetHeight(self.searchBar.bounds);
        [self.searchBar resignFirstResponder];
    }
}

#pragma mark UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterTableViewWithText:searchText];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self filterTableViewWithText:searchBar.text];
    [self.searchBar resignFirstResponder];
}

- (void)filterTableViewWithText:(NSString *)searchText
{
    if ([searchText isEqualToString:@""])
    {
        self.searchBarTopConstraint.constant = -CGRectGetHeight(self.searchBar.bounds);
        self.pageNumber = 1;
        [self.moviesTableView reloadData];
        [self.searchBar resignFirstResponder];
        [self.searchBar endEditing:YES];

    }
    else
    {
        self.movies = nil;
        [self loadMoreSearchMoviesFromPage:@1];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.movies.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    MovieTableViewCell *movieCell = (MovieTableViewCell *)cell;
    [movieCell configureWithModel:self.movies[indexPath.row]];
    movieCell.selectionStyle = UITableViewCellSelectionStyleNone;

    CGFloat maxPageKoef = self.movies.count/(self.pageNumber*20); //20 is number of moview on 1 page
    if (indexPath.row == self.movies.count-1 && maxPageKoef >=1)
    {
        self.pageNumber ++;
        if (self.searchBar.text.length)
        {
            [self loadMoreSearchMoviesFromPage:[NSNumber numberWithInteger:self.pageNumber]];
        }
        else
        {
            [self loadMoreTopMoviesFromPage:[NSNumber numberWithInteger:self.pageNumber]];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MovieTableViewCell *cell =  (MovieTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"movieCell" forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Movie *selectedMovie = self.movies[indexPath.row];
    [self performSegueWithIdentifier:kDetailVCSegueIdentifier sender:selectedMovie];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    if ([segue.identifier isEqualToString:kDetailVCSegueIdentifier])
    {
        MovieDetailVC *viewController = (MovieDetailVC*)segue.destinationViewController;
        viewController.movie = sender;
    }
}

@end
