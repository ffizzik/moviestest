//
//  MovieTableViewCell.m
//  MoviesTest
//
//  Created by Eugene Fozekosh on 1/30/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

#import "MovieTableViewCell.h"
#import "Movie.h"
#import "NetworkManager.h"
#import <UIImageView+AFNetworking.h>

static NSString *const kImageBaseURL = @"https://image.tmdb.org/t/p/w150";
static NSString *const kBiggerImageBaseURL = @"https://image.tmdb.org/t/p/w500";

@interface MovieTableViewCell ()

@property (strong, nonatomic) IBOutlet UIImageView *movieThumbnailImageView;
@property (strong, nonatomic) IBOutlet UILabel *movieNameLabel;

@end

@implementation MovieTableViewCell


- (void)configureWithModel:(Movie *)movie
{
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", kImageBaseURL, movie.posterPath]];
    
    __weak MovieTableViewCell *weakSelf = self;

    [self.movieThumbnailImageView setImageWithURLRequest:[NSURLRequest requestWithURL:imageURL] placeholderImage:[UIImage imageNamed:@"placeholder"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        weakSelf.movieThumbnailImageView.image = image;
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        
        //there no small poster for the movie, we need load bigger
        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", kBiggerImageBaseURL, movie.posterPath]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.movieThumbnailImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder"] ];
        });
    }];
    self.movieNameLabel.text = [NSString stringWithFormat:@"%@ (%@)",movie.title, movie.releaseDate];
}


@end
