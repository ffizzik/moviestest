//
//  MovieDetailVC.h
//  MoviesTest
//
//  Created by Eugene Fozekosh on 1/30/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Movie;

@interface MovieDetailVC : UIViewController

@property(nonatomic, strong) Movie *movie;

@end
