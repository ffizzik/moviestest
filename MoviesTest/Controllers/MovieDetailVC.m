//
//  MovieDetailVC.m
//  MoviesTest
//
//  Created by Eugene Fozekosh on 1/30/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

#import "MovieDetailVC.h"
#import "Movie.h"
#import <UIImageView+AFNetworking.h>

static NSString *const kOriginalImageBaseURL = @"https://image.tmdb.org/t/p/original";

@interface MovieDetailVC ()

@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *posterImageView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation MovieDetailVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    CGFloat totalHeight = CGRectGetHeight(self.posterImageView.frame) + CGRectGetHeight(self.titleLabel.frame) + CGRectGetHeight(self.descriptionTextView.frame) + 60.0; // 60.0 is total spacing between 3 views
    [self.scrollView setContentSize:CGSizeMake(CGRectGetWidth(self.view.frame), totalHeight)];
}

- (void)configureView
{
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", kOriginalImageBaseURL, self.movie.posterPath]];
    [self.posterImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder"] ];
    self.titleLabel.text = self.movie.title;
    self.descriptionTextView.text = self.movie.overview;
    self.descriptionTextView.translatesAutoresizingMaskIntoConstraints = YES;
    [self.descriptionTextView sizeToFit];
}

@end
