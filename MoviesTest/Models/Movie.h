//
//  Movie.h
//  MoviesTest
//
//  Created by Eugene Fozekosh on 1/30/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject

@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *identifier;
@property(nonatomic, strong) NSString *posterPath;
@property(nonatomic, strong) NSString *overview;
@property(nonatomic, strong) NSString *releaseDate;


- (void)updateFromDictionary:(NSDictionary*)dict;

@end
