//
//  Movie.m
//  MoviesTest
//
//  Created by Eugene Fozekosh on 1/30/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

#import "Movie.h"

@implementation Movie

- (void)updateFromDictionary:(NSDictionary*)dict
{
    self.title = [self safeStringFrom:dict[@"title"]];
    self.identifier = [self safeStringFrom:dict[@"id"]];
    self.posterPath = [self safeStringFrom:dict[@"poster_path"]];
    self.overview = [self safeStringFrom:dict[@"overview"]];
    self.releaseDate = [self getDateFromString:dict[@"release_date"]];
}

-(NSString*)getDateFromString:(NSString*)dateString
{
    NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd'"];
    NSDate *date = [formatter dateFromString:dateString];
    if (date)
    {
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:date];
        NSString *result = [NSString stringWithFormat:@"%ld", (long)components.year];
        return result;
    }
    else
    {
        return @"";
    }
}

- (NSString*)safeStringFrom:(NSString*) string
{
    if (string == nil || [string isKindOfClass:[NSNull class]])
    {
        return @"";
    }
    return string;
}

@end
