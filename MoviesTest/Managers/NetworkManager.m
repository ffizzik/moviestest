//
//  NetworkManager.m
//  MoviesTest
//
//  Created by Eugene Fozekosh on 1/30/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

#import "NetworkManager.h"
#import <AFNetworking.h>
#import "Movie.h"

static NSString *const kBaseURL = @"https://api.themoviedb.org/3";
static NSString *const kTopRatedURL = @"movie/top_rated?";
static NSString *const kSearchMovieURL = @"search/movie?";

static NSString *const kAPIKey = @"api_key=c33e25174af866c5c102772d92d0e480";

@interface NetworkManager ()

@property(strong, nonatomic) AFHTTPSessionManager *manager;
@end

@implementation NetworkManager


+ (instancetype)sharedInstance
{
    static NetworkManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[NetworkManager alloc] init];
    });
    return sharedInstance;
}

- (void)getMoviesWithSearchQuery:(NSString*)searchQuery page:(NSNumber*)number completion:(void(^)(NSArray *dataArray))completion
{
    NSString *url = [NSString stringWithFormat:@"%@/%@%@", kBaseURL, kSearchMovieURL, kAPIKey];
    NSDictionary *params = @{@"page":number,
                             @"query":searchQuery
                             };
        
    [[self manager] GET:url parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSError *jsonError = nil;
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&jsonError];
        if (!jsonError)
        {
            completion([self itemsFromArray:result[@"results"]]);
        }
        else
        {
            completion(nil);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}

- (void)getTopRatedMoviesWithPage:(NSNumber*)number completion:(void(^)(NSArray *dataArray))completion
{
    NSString *url = [NSString stringWithFormat:@"%@/%@%@", kBaseURL, kTopRatedURL, kAPIKey];
    
    NSDictionary *params = @{@"page":number};

    [[self manager] GET:url parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSError *jsonError = nil;
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&jsonError];

        if (!jsonError)
        {
            completion([self itemsFromArray:result[@"results"]]);
        }
        else
        {
            completion(nil);
        }

    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (AFHTTPSessionManager*)manager
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", nil];
    return manager;
}

- (NSArray*)itemsFromArray:(NSArray*)inputArray
{
    NSMutableArray *resultArray = [NSMutableArray new];
    [inputArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([[obj class] isSubclassOfClass:[NSDictionary class]])
        {
            Movie *movie = [[Movie alloc] init];
            [movie updateFromDictionary:obj];
            [resultArray addObject:movie];
        }
    }];
    return resultArray;
}

@end
