//
//  NetworkManager.h
//  MoviesTest
//
//  Created by Eugene Fozekosh on 1/30/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject

+ (instancetype)sharedInstance;

- (void)getTopRatedMoviesWithPage:(NSNumber*)number completion:(void(^)(NSArray *dataArray))completion;
- (void)getMoviesWithSearchQuery:(NSString*)searchQuery page:(NSNumber*)number completion:(void(^)(NSArray *dataArray))completion;

@end
